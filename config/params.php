<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'maxlapko@gmail.com',
    'user.passwordResetTokenExpire' => 10800,
];
