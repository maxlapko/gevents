window.APP = (function ($) {
    var Event = {        
        listing: function($parent) {
            $parent.on('click', '.deny', function(e) {
                e.preventDefault();
                var $el = $(this);
                if ($el.hasClass('disabled')) return;
                $el.addClass('disabled');
                APP.event.openPopup($el);
            }).on('click', '.apply', function(e) {
                e.preventDefault();
                var $el = $(this);
                if ($el.hasClass('disabled')) return;
                $el.addClass('disabled');
                $.post($el.attr('href'), APP.event.process(), 'json');
            });
            if (typeof(EVENT_ID) !== 'undefined') {
                $parent.find('.event').fadeOut(500).filter($('#event-' + EVENT_ID)).stop().addClass('warning').fadeIn();
            }
        },
        view: function($parent) {
            $parent.on('click', '.comment', function(e) {
                e.preventDefault();
                var $el = $(this);
                if ($el.hasClass('disabled')) return;
                $el.addClass('disabled');
                APP.event.openPopup($el);
            })
        },
        deny: function($form) {
            $form.find('button.btn').button('loading');
            $.post($form.attr('action'), $form.serialize(), APP.event.process($form), 'json');
            return false;
        },
        apply: function($form) {
            $form.find('button.btn').button('loading');
            $.post($form.attr('action'), $form.serialize(), APP.event.process($form, 'member'), 'json');
            return false;
        },
        process: function(form, prefix) {
            prefix = prefix || 'event';
            return function(rsp) {
                if (rsp.success) {
                    $('#main-modal').modal('hide');
                    var id = '#' + prefix + '-' + rsp.id;
                    $(id).html($(id, '<table>' + rsp.html + '</table>').html());
                    if (form) {
                        form[0].reset();
                        form.find('button.btn').button('reset');
                    }
                }
            }
        },
        openPopup: function($el) {
            $('#main-modal').data('bs.modal', null).off('hide.bs.modal').on('hidden.bs.modal', function(e) {
                $el.removeClass('disabled');
            }).modal({remote: $el.attr('href')});
        }
    };
    return {    
        init: function() {
            if ($('#events').length > 0) {
                this.event.listing($('#events'));
            }
            if ($('#members').length > 0) {
                this.event.view($('#members'));
            }
        },
        event: Event
    }
})(jQuery);

$(function() {
    APP.init();
})
