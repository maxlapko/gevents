<?php

use app\models\Member,
    yii\helpers\Html,
    yii\helpers\Url;

$app = Yii::$app;
$expired = $model->isExpired();
$user = $app->getUser();

?>
<tr class="event <?= $expired ? 'expired' : ''?>" id="event-<?= $model->id ?>">
    <td class="title">
        <h2><?= Html::a(Html::encode($model->title), ['/events/view', 'id' => $model->id], ['title' => Html::encode($model->title)]) ?></h2>
    </td>
    <td class="date"><?= $app->getFormatter()->asDate($model->published_at, 'full') ?></td>    
    <td class="control">
        <?php if (!$expired && !$user->getIsGuest()) : ?>
            <?php 
                $apply = $model->isMemberStatus($user->getIdentity(), Member::STATUS_APPLY);
                $deny = $model->isMemberStatus($user->getIdentity(), Member::STATUS_DENY);
            ?>
            
            <?= Html::a('', $apply ? '#' : ['/events/apply', 'id' => $model->id], [
                'class' => 'apply' . ($apply ? ' disabled' : ''),
                'title' => 'Буду!'
            ]) ?>
            &nbsp;&nbsp;
            <?= Html::a('', $deny ? '#' : ['/events/deny', 'id' => $model->id], [
                'class' => 'deny' . ($deny ? ' disabled' : ''),
                'title' => 'Есть дела поважнее'
            ]) ?>
        <?php endif ?>
    </td>    
</tr>



