<?php

use yii\helpers\Html,
    yii\widgets\DetailView;

/**
 * @var yii\web\View $this 
 * @var app\models\Event $model
 */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$members = $model->members;
?>
<div class="event-view">
    <div class="pull-right">
    <?php if (Yii::$app->getUser()->getId() == $model->user_id) : ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Согласны удалить это событие?',
                'method' => 'post',
            ],
        ]) ?>
    <?php else : ?>
        <strong><?= Html::encode($model->user->username)?></strong>
    <?php endif; ?>
    </div>
    <h1><?= Html::encode($this->title) ?> <small><?= Yii::$app->getFormatter()->asDate($model->published_at) ?></small></h1>
    
    <p><?= nl2br(Html::encode($model->comment))?></p>
    
    <?php 
        $stat = ['apply' => 0, 'deny' => 0];
    ?>
    <table class="table" id="members">
        <thead>
            <tr>
                <th>Имя</th>
                <th>Комментарий</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $i => $user) : ?>
                <?php 
                    $member = null;
                    if (isset($members[$user['id']])) {
                        $member = $members[$user['id']];
                        $stat[$member->status]++;
                    }
                ?>
                <?= $this->render('member/_view', [
                    'member' => $member, 
                    'model' => $model, 
                    'user' => $user
                ]) ?>
            <?php endforeach; ?> 
        </tbody>
    </table>
    <div class="well pull-right" style="width: 200px;">
        За: <?= $stat['apply'] ?>, против: <?= $stat['deny'] ?>, всего: <?= count($users) ?>
    </div>
</div>