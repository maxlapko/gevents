<?php

use yii\helpers\Html,
    kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Event $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Комментарий</h4>
</div>
<?php $form = ActiveForm::begin([
    'options' => ['id' => 'deny-form'],
    'beforeSubmit' => 'APP.event.deny'
]); ?>
<div class="modal-body">
    <?= $form->field($model, 'reason')->textarea() ?>    
</div>
<div class="modal-footer">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-danger']) ?>
</div>
<?php ActiveForm::end(); ?>

    
