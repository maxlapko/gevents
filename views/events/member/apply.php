<?php

use yii\helpers\Html,
    kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Event $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Ваш комментарий</h4>
</div>
<?php $form = ActiveForm::begin([
    'options' => ['id' => 'apply-form'],
    'beforeSubmit' => 'APP.event.apply'
]); ?>
<div class="modal-body">
    <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>    
</div>
<div class="modal-footer">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

    
