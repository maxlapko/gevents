<?php

use app\models\Member,
    yii\helpers\Html;

$isMember = isset($member);
?>

<tr id="member-<?= $user['id']?>" class="member <?= $isMember ? ($member->status === Member::STATUS_DENY ? 'danger' : 'apply') : 'pending' ?>">
    <td class="username"><?= Html::encode($user['username']) ?></td>
    <td>
        <?php if ($isMember) : ?>
            <?php 
                $field = $member->status === Member::STATUS_APPLY ? 'comment' : 'reason';
            ?>
            <?= Html::encode($member->$field) ?>
            <?php if (Yii::$app->getUser()->id == $member->user_id && !$model->isExpired() && $member->status === Member::STATUS_APPLY) : ?>
                <?= Html::a(($member->$field == '' ? 'Добавить комментарий' : '<span class="glyphicon glyphicon-pencil"></span>'), ['/events/apply', 'id' => $model->id, 'force' => 0], ['class' => 'comment']) ?>
            <?php endif ?>
        <?php endif; ?>
    </td>
</tr>
