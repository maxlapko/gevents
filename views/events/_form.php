<?php

use kartik\widgets\DatePicker,
    yii\helpers\Html,
    kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Event $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'published_at')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy',
            'startDate' => date('U')
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>
    <?= $form->field($model, 'present')->checkbox() ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
