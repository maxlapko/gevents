<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Все события';
$this->params['breadcrumbs'][] = 'События';
if ($eventId = Yii::$app->request->get('eventId', false)) {
    $this->registerJs('EVENT_ID = ' . (int) $eventId. ';', $this::POS_BEGIN);
}
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать событие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['tag' => false],
        'itemView' => '_view',
        'layout' => "{summary}\n<div id=\"events\"><table class=\"table\"><tbody>{items}</tbody></table></div>\n{pager}"
    ]) ?>

</div>
