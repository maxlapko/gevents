<?php

namespace app\models\query;

use app\models\User,
    yii\db\ActiveQuery;

/**
 * Description of UserQuery
 *
 * @author mlapko
 */
class UserQuery extends ActiveQuery
{
    /**
     * @param type $status
     * @return UserQuery
     */
    public function withStatus($status = User::STATUS_ACTIVE)
    {
        $this->andWhere(['status' => $status]);
        return $this;
    }
}
