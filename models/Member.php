<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_events".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property string $status
 * @property string $comment
 * @property string $reason
 * @property string $created_at
 * @property string $updated_at
 */
class Member extends \yii\db\ActiveRecord
{
    const STATUS_APPLY = 'apply';
    const STATUS_DENY = 'deny';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_events';
    }
    
    public function scenarios()
    {
        return [
            'apply' => ['comment'],
            'deny' => ['reason'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['reason'], 'required', 'on' => 'deny'],
            [['comment', 'reason'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'event_id' => 'Event ID',
            'status' => 'Status',
            'comment' => 'Комментарий',
            'reason' => 'Причина отказа',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
