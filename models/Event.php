<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $published_at
 * @property string $created_at
 * @property string $updated_at
 */
class Event extends ActiveRecord
{
    public $present = true;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'published_at'], 'required'],
            [['published_at', 'present', 'comment'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['event_id' => 'id'])->with('user')->indexBy('user_id');
    }
    
    public function getMemberCount()
    {
        return Member::find(['event_id' => $this->id])->count();
    }
    
    /**
     * @param User $user
     * @return boolean
     */
    public function isMember($user)
    {
        return $this->getMember($user) !== null;
    }
    
    /**
     * @param User $user
     * @param string $status
     * @return boolean
     */
    public function isMemberStatus($user, $status)
    {
        $m = $this->getMember($user);
        return $m !== null && $m->status === $status;
    }
    
    
    
    /**
     * @staticvar array $cache
     * @param type $user
     * @return Member
     */
    public function getMember($user)
    {
        static $cache = [];
        $key = $this->id . '-' . $user->id;
        if (!isset($cache[$key])) {
            $cache[$key] = Member::findOne(['event_id' => $this->id, 'user_id' => $user->id]);
        }
        return $cache[$key];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Название',
            'comment' => 'Комментарий',
            'published_at' => 'Дата',
            'present' => 'Буду!',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function isExpired()
    {
        return date('Y-m-d') > $this->published_at;
    }
    
    public function apply($user, $member = null)
    {
        if ($member === null) {
            $member = new Member(['scenario' => Member::STATUS_APPLY]);
        }
        $member->user_id = $user->id;
        $member->event_id = $this->id;
        $member->status = Member::STATUS_APPLY;
        return $member->save();
    }
    
    public function deny($user, $member)
    {
        $member->user_id = $user->id;
        $member->event_id = $this->id;
        $member->status = Member::STATUS_DENY;
        return $member->save();
    }
    
    
    public function beforeSave($insert)
    {
        $this->published_at = date('Y-m-d', strtotime($this->published_at));
        return parent::beforeSave($insert);
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        Member::deleteAll(['event_id' => $this->id]);
    }
    
}
