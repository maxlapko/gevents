<?php

namespace app\controllers;

use app\models\Event,
    app\models\Member,
    app\models\User,
    Yii,
    yii\data\ActiveDataProvider,
    yii\filters\AccessControl,
    yii\filters\VerbFilter,
    yii\helpers\Json,
    yii\web\Controller,
    yii\web\ForbiddenHttpException,
    yii\web\NotFoundHttpException;

/**
 * EventsController implements the CRUD actions for Event model.
 */
class EventsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'deny'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'deny'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Event::find()->with('user')->orderBy('published_at DESC');
        
        $user = null;
        if ($userId = Yii::$app->request->get('userId', false) && $user = User::findOne($userId)) {
            $query->andWhere('user_id = :userId', [':userId' => $user->id]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id), 
            'users' => User::find()->withStatus(User::STATUS_ACTIVE)->asArray()->all()
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event;
        $model->user_id = Yii::$app->user->getId();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->present) {
                $model->apply(Yii::$app->getUser()->getIdentity());
            }
            $this->sendEmails($model);
            Yii::$app->getSession()->setFlash('success', 'Событие было успешно создано.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if (Yii::$app->getUser()->getId() != $model->user_id) {
            throw new ForbiddenHttpException('Hello Radja');
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->getUser()->getId() != $model->user_id) {
            throw new ForbiddenHttpException('Hello Radja');
        }
        $model->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeny($id)
    {
        $event = $this->findModel($id);
        $user = Yii::$app->getUser()->getIdentity();
        $model = $event->getMember($user);
        $model = $model ? : new Member;
        $model->setScenario(Member::STATUS_DENY);
        
        if ($model->load(Yii::$app->request->post()) && $event->deny($user, $model)) {
            return Json::encode([
                'success' => true,
                'html' => $this->renderPartial('_view', ['model' => $event]),
                'id' => $event->id
            ]);
        } else {
            return $this->renderAjax('member/deny', ['model' => $model]);
        }
    }
    
    public function actionApply($id, $force = 1)
    {
        $event = $this->findModel($id);
        $user = Yii::$app->getUser()->getIdentity();
        $model = $event->getMember($user);
        $model = $model ? : new Member;
        $model->setScenario(Member::STATUS_APPLY);
        
        if (($force || $model->load(Yii::$app->request->post())) && $event->apply($user, $model)) {
            return Json::encode([
                'success' => true,
                'html' => $force ? 
                    $this->renderPartial('_view', ['model' => $event]) : 
                    $this->renderPartial('member/_view', ['member' => $model, 'model' => $event, 'user' => $user->toArray()]),
                'id' => $force ? $event->id : $model->user_id
            ]);
        } else {
            return $this->renderAjax('member/apply', ['model' => $model]);
        }
    }
    
    /**
     * @param \app\models\Event $event
     */
    public function sendEmails($event)
    {
        $users = User::find(['status' => User::STATUS_ACTIVE])->indexBy('id')->asArray()->all();
        unset($users[$event->user_id]);
        if (empty($users)) {
            return;
        }
        
        $mailer = Yii::$app->mail;
        foreach ($users as $user) {
            $mailer->compose('new-event', ['user' => $user, 'event' => $event])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($user['email'])
                ->setSubject('Новое событие: ' . \yii\helpers\Html::encode($event->title))
                ->send();
        }
        return true;
    }
    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
