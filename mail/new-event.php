<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Event $event
 * @var array $user
 */

$link = Yii::$app->urlManager->createAbsoluteUrl(['/events/index', 'eventId' => $event->id]);
?>

Привет <?= Html::encode($user['username']) ?>, <br /><br />

<b><?= Html::encode($event->user->username) ?></b> cоздал(а) новое событие: <br />

<h3><?= Html::encode($event->title)?></h3>
<p>
    Дата: <b><?= Yii::$app->getFormatter()->asDate($event->published_at, 'full') ?></b><br />
    Комментарий: <?= nl2br(Html::encode($event->comment))?>
</p>
<p>Страница для голосования: <?= Html::a('Посмотреть', $link) ?></p>